namespace Blog_t6_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(nullable: false),
                        Email = c.String(),
                        FirstName = c.String(maxLength: 100),
                        LastName = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.Posts", "AccountID", c => c.Int(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.Tags", "Context", c => c.String(nullable: false, maxLength: 100));
            AddForeignKey("dbo.Posts", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
            CreateIndex("dbo.Posts", "AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "AccountID" });
            DropForeignKey("dbo.Posts", "AccountID", "dbo.Accounts");
            AlterColumn("dbo.Tags", "Context", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            DropColumn("dbo.Posts", "AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
