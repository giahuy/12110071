namespace Blog_t6_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Posts", "Body", c => c.String(maxLength: 2000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Body", c => c.String(maxLength: 250));
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false));
        }
    }
}
