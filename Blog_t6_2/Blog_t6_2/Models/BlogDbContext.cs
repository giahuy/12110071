﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog_t6_2.Models
{
    public class BlogDbContext:DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }
        public DbSet<Account> Accounts { set; get; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>()
                .HasMany(d => d.Tags).WithMany(p => p.Posts)
                .Map(t => t.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Tag_Post"));
        }

        public DbSet<Tag> Tags { get; set; }
    }
}