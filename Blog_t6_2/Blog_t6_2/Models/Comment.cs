﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_t6_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        [StringLength(1000,MinimumLength=50,ErrorMessage="Nhập tối thiểu là 50 kí tự!")]
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        public DateTime DateUpdate { set; get; }
        public string Author { set; get; }
        public int PostID { set; get; }
        public virtual Post post { set; get; }
    }
}