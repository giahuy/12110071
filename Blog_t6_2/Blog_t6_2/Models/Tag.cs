﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_t6_2.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required]
        [StringLength(100,MinimumLength=10,ErrorMessage="Từ 20 đến  100 kí tự!")]
        public string Context { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}