﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog5_t6.Models
{
    public class Post
    {
        public int ID { set; get; }
        // [Required]
        //[StringLength(500, ErrorMessage = "Số kí trong khoảng 5->500 kí tự!", MinimumLength = 5)]
        public string Title { set; get; }
        // [StringLength(2000, ErrorMessage = "Tối thiểu 5 kí tự!", MinimumLength = 5)]
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        // [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày giờ!!!")]
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserID { set; get; }
    }
}